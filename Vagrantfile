# -*- mode: ruby -*-
# vi: set ft=ruby :

ENV['VAGRANT_NO_PARALLEL'] = 'yes'

Vagrant.configure(2) do |config|

  config.vm.provision "shell", path: "node-setup.sh"

  # Kubernetes Master Server
  config.vm.define "master" do |master|
    master.vm.box = "ubuntu/xenial64"
    config.vm.box_version = "20200320.0.0"
    master.vm.hostname = "master.example.com"
    master.vm.network "private_network", ip: "192.168.1.40"
    master.vbguest.auto_update = false
    master.vm.network :forwarded_port, guest: 30001, host: 8080
    master.vm.synced_folder "./values", "/home/vagrant"
    master.vm.provider "virtualbox" do |v|
      v.name = "master"
      v.memory = 2048
      v.cpus = 2
       #Prevent VirtualBox from interfering with host audio stack
      v.customize ["modifyvm", :id, "--audio", "none"]
    end
    master.vm.provision "shell", path: "master-setup.sh"
  end

  NodeCount = 2

  # Kubernetes Worker Nodes
  (1..NodeCount).each do |i|
    config.vm.define "worker#{i}" do |workernode|
      workernode.vm.box = "ubuntu/xenial64"
      config.vm.box_version = "20200320.0.0"
      workernode.vm.hostname = "worker#{i}.example.com"
      workernode.vm.network "private_network", ip: "192.168.1.4#{i}"
      workernode.vbguest.auto_update = false
      #workernode.vm.synced_folder "./palindrome", "/srv/app/palindrome"
      workernode.vm.provider "virtualbox" do |v|
        v.name = "worker#{i}"
        v.memory = 1024
        v.cpus = 1
        # Prevent VirtualBox from interfering with host audio stack
        v.customize ["modifyvm", :id, "--audio", "none"]
      end
      workernode.vm.provision "shell", path: "worker-setup.sh"
    end
  end

end
