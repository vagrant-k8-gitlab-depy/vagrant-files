# Vagrant File to provition a Kubernetes Cluster

This is a basic vagrant file that deploys a Kubernetes cluster on Virtualbox) using vagrant and shell scripts.

## Getting Started

The instructions should get you up and running on your local machine for development and testing purposes.

### Pre-requisites

#### Windows
1. Downlad virtualbox from https://www.virtualbox.org/wiki/Downloads (select windows host).

2. Install Virtualbox
   ```
   Open the .exe file once it has completed downloading and follow the the installation instructions
   ```
3. Install Vagrant
   ```
   Download the .exe app from https://www.vagrantup.com/downloads.Open and follow instructions.

#### Macos
1. Install [Homebrew](https://brew.sh/)

2. Install Virtualbox
   ```
   $ brew cask install virtualbox
   ```
3. Install Vagrant
   ```
   $ brew cask install vagrant
    ```
#### Linux
- Use the appropriate package manager for your distribution
1. Install Virtualbox
   ```
   $ sudo apt install virtualbox   ```

2. Install Vagrant
   ```
   $ sudo apt update
   $ curl -O https://releases.hashicorp.com/vagrant/2.2.6/vagrant_2.2.6_x86_64.deb
   $ sudo apt install ./vagrant_2.2.6_x86_64.deb
   ```
3. Check version vagrant --version

## Installing
* git clone the vagrant repo
  ```
  $ https://gitlab.com/vagrant-k8-gitlab-depy/vagrant-files.git
  ```  
* Run Vagrant to setup the cluster from your terminal.
  ```sh
  $ vagrant up
  ```
* Apply the service Kubernetes manifest, to create a network service that exposes the application.
  ```sh
  $ vagrant ssh master 
  ```
  Then run kubectl apply -f /home/vagrant/service-deployment.yml
  ```

##Verify the installation

1. Check the docker version 
   ```sh
   $ docker --version
2. Check for available pods.
   ```sh
   $ kubectl get pods

3. Get the cluster information
   ```sh
   $ kubectl cluster-info

4. Check for available pods. $kubectl get nodes
   ```sh
      |NAME    | STATUS|  ROLES | AGE | VERSION |
      |--------|-------|--------|-----|---------|
      |master  | Ready | master | 55m | v1.18.3 |
      |worker1 | Ready | <none> | 27m | v1.18.3 |
      |worker2 | Ready | <none> | 27m | v1.18.3 |
   ```

5. Created Gitlab runner for pull-requests and pushes to master. 
   ```
   under settings -> ci/cd/runner
   ```
## What the installation does

1. Provitions three virtual machines for the Kubernetes cluster, one for master and the remaining two for worker nodes.
2. Install a gitlab runner on the master node
3. Installs Docker on the three machines.
4. Install k8 and configures a network that exposes the pods
5. Created Github Actions workflows for pull-requests and pushes to master. 
6. The vagrant file should set up everything,once verything is up and running view the application, visit (http://localhost:8080//api/v1/palindrome/mam)
   
   ```
   The set up might take a few minute depending with you internet speed

