#!/bin/bash

# Initialize Kubernetes
echo "[TASK 1] set hostnamectl"
# set host name
hostnamectl set-hostname master

# Initialize Kubernetes
echo "[TASK 2] Initialize Kubernetes Cluster"
kubeadm init --apiserver-advertise-address=192.168.1.40 --pod-network-cidr=192.168.0.0/16 >>/root/kubeinit.log

# Copy Kube admin config
echo "[TASK 3] Copy kube admin config to Vagrant user .kube directory"
mkdir -p /home/vagrant/.kube
cp /etc/kubernetes/admin.conf /home/vagrant/.kube/config
chown $(id -u vagrant):$(id -g vagrant) /home/vagrant/.kube/config -R

# Ddeploy a pod network
echo "[TASK 4] Deploy Calico network"
#do manually
export KUBECONFIG=/etc/kubernetes/admin.conf
kubectl apply -f https://docs.projectcalico.org/v3.11/manifests/calico.yaml
#su - vagrant -c "kubectl apply -f https://docs.projectcalico.org/v3.11/manifests/calico.yaml"

# Generate Cluster join command
echo "[TASK 5] Generate and save cluster join command to /joincluster.sh"
kubeadm token create --print-join-command >>/etc/clustertoken.sh
chmod +x /etc/clustertoken.sh

#install helm on the master
echo "[TASK 6] install helm on the master"
#curl -L https://git.io/get_helm.sh | bash >/dev/null 2>&1
#curl https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 | bash

#add gitrepo
echo "[Task 7] add gitlab https://charts.gitlab.io"
#helm repo add gitlab https://charts.gitlab.io

#run this manually
echo "[Task 8] create service"
#su - vagrant -c  "kubectl -c apply -f service-deployment.yml"

#run this manually
echo "[Task 9] create namespace"
#su - vagrant -c  "kubectl create namespace palindrome"

#run helm #run this manually
echo "[Task 10] run helm su - vagrant -c "
#su - vagrant -c "helm install gitlab-runner -f values.yaml gitlab/gitlab-runner"

# required for setting up password less ssh between guest VMs
sed -i "/^[^#]*PasswordAuthentication[[:space:]]no/c\PasswordAuthentication yes" /etc/ssh/sshd_config
service sshd restart

#Docker runner
echo "[TASK 1] build image"
#docker volume create gitlab-runner-config


#docker run -d --name gitlab-runner --restart always \
#    -v /srv/gitlab-runner/config:/etc/gitlab-runner \
 #    -v /var/run/docker.sock:/var/run/docker.sock \
  #  gitlab/gitlab-runner:latest

#echo "[TASK 3] register the runner"
#sudo docker run --rm -v /srv/gitlab-runner/config:/etc/gitlab-runner gitlab/gitlab-runner register \
# --non-interactive \
# --executor "docker" \
 #--docker-image alpine:latest \
 #--url "https://gitlab.com/" \
# --registration-token "Lq5jyozzcmJuvN7HyW3d" \
# --description "docker-runner" \
# --tag-list "docker,vagrant,kubernetes,palindrome" \
# --run-untagged="true" \
# --locked="false" \
# --access-level="not_protected"

curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64

chmod +x /usr/local/bin/gitlab-runner

useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash

gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner

gitlab-runner start

sudo gitlab-runner register \
  --non-interactive \
  --url "https://gitlab.com/" \
  --registration-token "Lq5jyozzcmJuvN7HyW3d" \
  --executor "shell" \
  --docker-image alpine:latest \
  --description "shell-runner" \
  --tag-list "palindrome" \
  --run-untagged="true" \
  --locked="false" \
  --access-level="not_protected"

sudo usermod -a -G sudo gitlab-runner
