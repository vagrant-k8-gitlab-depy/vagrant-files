#!/bin/bash

# Update hosts file
echo "[TASK 1] Update /etc/hosts file"
cat >>/etc/hosts<<EOF
192.168.1.40 master.example.com master
192.168.1.41 worker1.example.com worker1
192.168.1.42 worker2.example.com worker2
EOF

# Install docker from Docker-ce repository
echo "[TASK 2] Install docker container engine required packages"
# (Install Docker CE)
## Set up the repository
### Install required packages
apt update
apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common >/dev/null 2>&1
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

## Add the Docker repository
echo "[TASK 3] Add the Docker repository"
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/$(. /etc/os-release; echo "$ID") $(lsb_release -cs) stable" >/dev/null 2>&1

# Install Docker CE
echo "[TASK 4] Install Docker CE"
apt-get update && apt-get install -y docker-ce docker-ce-cli containerd.io >/dev/null 2>&1


echo "[TASK 5] Set up the Docker daemon"
## Create /etc/docker
mkdir /etc/docker
# Set up the Docker daemon
cat > /etc/docker/daemon.json <<EOF
{
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m"
  },
  "storage-driver": "overlay2",
  "storage-opts": [
    "overlay2.override_kernel_check=true"
  ]
}
EOF

#mkdir -p /etc/systemd/system/docker.service.d
usermod -aG docker vagrant

# Restart Docker
echo "[TASK 6] Start/Restart Docker"
#systemctl enable docker >/dev/null 2>&1
systemctl daemon-reload
systemctl restart docker

# Disable SELinux
#echo "[TASK 7] Disable SELinux"
#setenforce 0
#sed -i --follow-symlinks 's/^SELINUX=enforcing/SELINUX=disabled/' /etc/sysconfig/selinux

# Stop and disable firewalld
#echo "[TASK 5] Stop and Disable firewalld"
#systemctl disable firewalld >/dev/null 2>&1
#systemctl stop firewalld

# Add sysctl settings
echo "[TASK 7] Add sysctl settings"
cat >>/etc/sysctl.d/kubernetes.conf<<EOF
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF
sysctl --system >/dev/null 2>&1

# Add yum repo file for Kubernetes
echo "[TASK 8] Add  repo file for kubernetes"
apt-get update && sudo apt-get install -y apt-transport-https curl
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
cat <<EOF | sudo tee /etc/apt/sources.list.d/kubernetes.list
deb https://apt.kubernetes.io/ kubernetes-xenial main
EOF


# Install Kubernetes
echo "[TASK 9] Install Kubernetes (kubeadm, kubelet and kubectl)"
apt-get update
apt-get install -y kubelet kubeadm kubectl >/dev/null 2>&1
apt-mark hold kubelet kubeadm kubectl

# Disable swap
echo "[TASK 10] Disable and turn off SWAP"
sed -i '/swap/d' /etc/fstab
swapoff -a
sed -i '/ swap / s/^\(.*\)$/#\1/g' /etc/fstab

# Start and Enable kubelet service
echo "[TASK 11] Enable and start kubelet service"
systemctl daemon-reload
systemctl restart kubelet

#systemctl enable kubelet >/dev/null 2>&1
#systemctl start kubelet >/dev/null 2>&1

# Enable ssh password authentication
echo "[TASK 11] Enable ssh password authentication"
sed -i 's/^PasswordAuthentication no/PasswordAuthentication yes/' /etc/ssh/sshd_config
systemctl reload sshd

# Set Root password
#echo "[TASK 12] Set root password"
#echo "cnjiru" | passwd --stdin root >/dev/null 2>&1

# Update vagrant user's bashrc file
echo "export TERM=xterm" >> /etc/bashrc

# Set nodeip of the box
#IP_ADDR=`ifconfig enp0s8 | grep Mask | awk '{print $2}'| cut -f2 -d:`
#KUBELET_CONFIG_FILE="/etc/default/kubelet"
#if [ ! -f "$KUBELET_CONFIG_FILE" ]
#then
#    touch "$KUBELET_CONFIG_FILE"
 #   echo KUBELET_EXTRA_ARGS=--node-ip="$IP_ADDR" >> "$KUBELET_CONFIG_FILE"
#else
#    sed -i "/^[^#]*KUBELET_EXTRA_ARGS=/c\KUBELET_EXTRA_ARGS=--node-ip=$IP_ADDR" "$KUBELET_CONFIG_FILE"
#fi
# Restart kubectl
#systemctl daemon-reload
#systemctl restart kubelet
